import pytest

from app import app
from app import jumlah

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client

def test_index(client):
    rv = client.get('/')
    assert b'Hi dunia' in rv.data

def test_jumlah():
    result = jumlah(3, 5)
    assert 8 == result
